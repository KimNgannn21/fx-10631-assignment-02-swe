# FUNiX Passport
- Tên dự án =: FUNiX Passport
- Mã sinh viên của người làm Assignment: Nguyễn Đặng Kim Ngân FX10631
- Repo này sử dụng cho mục đích: lưu trữ, thực hiện các chỉnh sửa mà không làm ảnh hưởng tới Original Repo. 
- Repo gồm các branch sau: 
    - bug/clear_console_log: xóa các câu lệnh 'console.log' trong script\subtitle\udemy-subtitle.js
    - feat/auto_enable_subtitle:sửa function pageLoad thành tự động trong script\subtitle\udemy-subtitle.js
    - document: tạo folder mới có tên 'documentation'. Trong đó, thêm file asm1.docx và thêm 1 folder tên "diagram" chứa các ảnh diagram của môn học. 
    - backend: tạo folder mới tên "backend". Trong đó, file mới là backend.md được thêm vào với một số định nghĩa xoay quanh backend.
- Các thông tin học viên muốn bổ sung cho Repo là: không có